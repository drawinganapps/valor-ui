class AgentModel {
  List<RelatedContentModel>? relatedContent;
  String? id;
  UrlModel? url;
  String? title;
  String? description;
  String? role;
  UrlModel? roleIcon;
  List<AbilitiesModel>? abilities;
  UrlModel? agentImage;
  List<AgentBackgroundVideoModel>? agentBackgroundVideo;

  AgentModel(
      {this.relatedContent,
      this.id,
      this.url,
      this.title,
      this.description,
      this.role,
      this.roleIcon,
      this.abilities,
      this.agentImage,
      this.agentBackgroundVideo});

  AgentModel.fromJson(Map<String, dynamic> json) {
    if (json['related_content'] != null) {
      relatedContent = <RelatedContentModel>[];
      json['related_content'].forEach((v) {
        relatedContent!.add(RelatedContentModel.fromJson(v));
      });
    }
    id = json['id'];
    url = json['url'] != null ? UrlModel.fromJson(json['url']) : null;
    title = json['title'];
    description = json['description'];
    role = json['role'];
    roleIcon =
        json['role_icon'] != null ? UrlModel.fromJson(json['role_icon']) : null;
    if (json['abilities'] != null) {
      abilities = <AbilitiesModel>[];
      json['abilities'].forEach((v) {
        abilities!.add(AbilitiesModel.fromJson(v));
      });
    }
    agentImage = json['agent_image'] != null
        ? UrlModel.fromJson(json['agent_image'])
        : null;
    if (json['agent_background_video'] != null) {
      agentBackgroundVideo = <AgentBackgroundVideoModel>[];
      json['agent_background_video'].forEach((v) {
        agentBackgroundVideo!.add(AgentBackgroundVideoModel.fromJson(v));
      });
    }
  }
}

class AgentBackgroundVideoModel {
  VideoModel? video;
  FileModel? staticImage;

  AgentBackgroundVideoModel({this.video, this.staticImage});

  AgentBackgroundVideoModel.fromJson(Map<String, dynamic> json) {
    video = json['video'] != null ? VideoModel.fromJson(json['video']) : null;
    staticImage = json['static_image'] != null
        ? FileModel.fromJson(json['static_image'])
        : null;
  }
}

class RelatedContentModel {
  String? machineName;
  String? title;
  UrlModel? url;
  String? description;
  String? createdAt;
  String? id;

  RelatedContentModel(
      {this.machineName,
      this.title,
      this.url,
      this.description,
      this.createdAt,
      this.id});

  RelatedContentModel.fromJson(Map<String, dynamic> json) {
    machineName = json['machine_name'];
    title = json['title'];
    url = json['url'] != null ? UrlModel.fromJson(json['url']) : null;
    description = json['description'];
    createdAt = json['created_at'];
    id = json['id'];
  }
}

class UrlModel {
  String? url;

  UrlModel({this.url});

  UrlModel.fromJson(Map<String, dynamic> json) {
    url = json['url'];
  }
}

class FileModel {
  String? url;

  FileModel({this.url});

  FileModel.fromJson(Map<String, dynamic> json) {
    url = json['url'];
  }
}

class AbilitiesModel {
  String? abilityDescription;
  String? abilityName;
  UrlModel? abilityIcon;
  List<AbilityVideoModel>? abilityVideo;

  AbilitiesModel(
      {this.abilityDescription,
      this.abilityName,
      this.abilityIcon,
      this.abilityVideo});

  AbilitiesModel.fromJson(Map<String, dynamic> json) {
    abilityDescription = json['ability_description'];
    abilityName = json['ability_name'];
    abilityIcon = json['ability_icon'] != null
        ? UrlModel.fromJson(json['ability_icon'])
        : null;
    if (json['ability_video'] != null) {
      abilityVideo = <AbilityVideoModel>[];
      json['ability_video'].forEach((v) {
        abilityVideo!.add(AbilityVideoModel.fromJson(v));
      });
    }
  }
}

class AbilityVideoModel {
  VideoModel? video;
  UrlModel? staticImage;

  AbilityVideoModel({this.video, this.staticImage});

  AbilityVideoModel.fromJson(Map<String, dynamic> json) {
    video = json['video'] != null ? VideoModel.fromJson(json['video']) : null;
    staticImage = json['static_image'] != null
        ? UrlModel.fromJson(json['static_image'])
        : null;
  }
}

class VideoModel {
  int? width;
  int? height;
  UrlModel? file;

  VideoModel({this.width, this.height, this.file});

  VideoModel.fromJson(Map<String, dynamic> json) {
    width = json['width'];
    height = json['height'];
    file = json['file'] != null ? UrlModel.fromJson(json['file']) : null;
  }
}

class AgentLocalizationModel {
  String? biographyLabel;
  String? dropdownPlaceholder;
  String? introDetail;
  String? introTitle;
  String? roleLabel;
  String? abilitiesLabel;

  AgentLocalizationModel(
      {this.biographyLabel,
      this.dropdownPlaceholder,
      this.introDetail,
      this.introTitle,
      this.roleLabel,
      this.abilitiesLabel});

  AgentLocalizationModel.fromJson(Map<String, dynamic> json) {
    final dynamic data = json['localization'];

    biographyLabel = data['biographyLabel'];
    dropdownPlaceholder = data['dropdownPlaceholder'];
    introDetail = data['introDetail'];
    introTitle = data['introTitle'];
    roleLabel = data['roleLabel'];
    abilitiesLabel = data['headline'];
  }
}

enum AgentRole { All, Controller, Duelist, Initiator, Sentinel }

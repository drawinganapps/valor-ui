class WeaponModel {
  String? weaponName;
  WeaponAssetModel? weaponAsset;
  String? weaponTagline;
  String? weaponCategoryMachineName;
  List<String>? weaponHoverDescription;

  WeaponModel(
      {this.weaponName,
        this.weaponAsset,
        this.weaponTagline,
        this.weaponCategoryMachineName,
        this.weaponHoverDescription});

  WeaponModel.fromJson(Map<String, dynamic> json) {
    weaponName = json['weapon_name'];
    weaponAsset = json['weapon_asset'] != null
        ? WeaponAssetModel.fromJson(json['weapon_asset'])
        : null;
    weaponTagline = json['weapon_tagline'];
    weaponCategoryMachineName = json['weapon_category_machine_name'];
    weaponHoverDescription = json['weapon_hover_description'].cast<String>();
  }
}

class WeaponAssetModel {
  String? url;
  String? title;

  WeaponAssetModel({this.url, this.title});

  WeaponAssetModel.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    title = json['title'];
  }
}

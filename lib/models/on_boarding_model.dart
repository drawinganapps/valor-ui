class OnBoardingModel {
  final String background;
  final String title;
  final String text;

  OnBoardingModel(this.title, this.text, this.background);
}
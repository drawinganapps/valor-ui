import 'package:valorant_ui/models/on_boarding_model.dart';

class DummyData {
  static String text1 = "Valorant agents are part of valorant protocols - a covert operation set up to prevent many disaster.";
  static String text2 = "Mirror agents wants to steal the Radianite from our Earth. Our agents come together to stop them.";
  static String text3 = 'Radianite steal process start with "Spike" activation. A company named kingdom own these devices.';

  static List<OnBoardingModel> onBoarding = [
    OnBoardingModel('Valorant Protocol', DummyData.text1, '2576136.png'),
    OnBoardingModel('Mirror Earth Agents', DummyData.text2, '3717295.jpg'),
    OnBoardingModel('Kingdom Corporation', DummyData.text3, '3911016.png'),
  ];
}

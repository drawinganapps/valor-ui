import 'package:flutter/material.dart';
import 'package:valorant_ui/models/agent_model.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color whiteDarker = const Color.fromRGBO(240, 240, 240, 1.0);
  static Color primary = const Color.fromRGBO(189,57,68, 1);
  static Color secondary = const Color.fromRGBO(203, 51, 62, 1.0);
  static Color dark = const Color.fromRGBO(15,25,35, 1);
  static Color lightDark = const Color.fromRGBO(19, 33, 46, 1);
  static Color lightDarkSecondary = const Color.fromRGBO(27, 42, 63, 1);
  static Color grey = const Color.fromRGBO(166,163,158, 1);

  static Color getBackgroundColorAgent(AgentRole agentRole) {
    switch(agentRole) {
      case AgentRole.Controller:
        return Colors.blueAccent;
      case AgentRole.Duelist:
        return Colors.greenAccent;
      case AgentRole.Sentinel:
        return Colors.orangeAccent;
      case AgentRole.Initiator:
        return Colors.redAccent;
      default:
        return Colors.redAccent;
    }
  }
}
